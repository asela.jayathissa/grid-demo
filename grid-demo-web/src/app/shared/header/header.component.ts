import { Component } from '@angular/core';
import { SidenavServiceService } from 'src/app/services/sidenav-service.service';

@Component({
  selector: 'xgy-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(
    private sidenav: SidenavServiceService) { }
    
    toggleRightSidenav() {
       this.sidenav.toggle();
    }
}
