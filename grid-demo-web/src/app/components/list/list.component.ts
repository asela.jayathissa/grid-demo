import { Component } from '@angular/core';
import { Employee, GridDataServiceService } from 'src/app/services/grid-data-service.service';

@Component({
  selector: 'xgy-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {
  employees: Employee[] = [];
 
  constructor(service: GridDataServiceService) {
      this.employees = service.getEmployees();
  }
}
