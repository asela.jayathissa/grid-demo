import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { DxDataGridModule } from 'devextreme-angular';



@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    DxDataGridModule
  ],
  exports: [
    ListComponent
  ]
})
export class ComponentsModule { }
